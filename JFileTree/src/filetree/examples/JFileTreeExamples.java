package filetree.examples;

import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.GridLayout;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import filetree.JFileTree;
import filetree.model.AvailableRootTypes;
import filetree.model.ComputerTreeModel;
import filetree.model.DataStore;
import filetree.model.TreeType;

/**
 * An implementation example that shows multiple different versions of JFileTree
 *
 * @version 1.0
 * @author Dan
 */
public class JFileTreeExamples extends JFrame {
	/**
	 * 
	 * @version 1.0
	 * @author Dan
	 */
	private static final long serialVersionUID = -23339031993806586L;
	
	/**
	 * 
	 * @version 1.0
	 * @author Dan
	 */
	private static final String NAME = "JFileTree Examples";
	
	/**
	 * 
	 * @version 1.0
	 * @author Dan
	 */
	private static final Dimension PANEL_SIZE = new Dimension(400, 600);
	
	/**
	 * 
	 * @version 1.0
	 * @author Dan
	 */
	private JFileTreeExamples() {
		super(NAME);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setLayout(new GridLayout());
		setUp();
		pack();
		setLocationRelativeTo(null);
		setVisible(true);
	}
	
	/**
	 * 
	 * @version 1.0
	 * @author Dan
	 */
	private void setUp() {
		AvailableRootTypes.setUpForWindows();
		JTabbedPane tabs = createAndSetUpTabs();
		getContentPane().add(createPanel(tabs));
	}
	
	/**
	 * 
	 * @param tabs
	 * @return
	 * @version 1.0
	 * @author Dan
	 */
	private JPanel createPanel(JTabbedPane tabs) {
		JPanel panel = new JPanel(new GridLayout(1, 1));
		panel.setPreferredSize(PANEL_SIZE);
		panel.add(tabs);
		return panel;
	}
	
	/**
	 * Creates a {@code JTabbedPane} that contains the different variations of {@code JFileTree}
	 * 
	 * @return Returns a {@code JTabbedPane} with 7 tabs. One for each variation of {@code JFileTree}
	 * @version 1.0
	 * @author Dan
	 */
	private JTabbedPane createAndSetUpTabs() {
		JTabbedPane tabs = new JTabbedPane();
		tabs.addTab("Alphabetical", createJFileTreePanel(TreeType.ALPHABETICAL, false));
		tabs.addTab("Alphabetical Extensions", createJFileTreePanel(TreeType.ALPHABETICAL, true));
		tabs.addTab("Dir First", createJFileTreePanel(TreeType.DIRS_FIRST, false));
		tabs.addTab("Dir First Extensions", createJFileTreePanel(TreeType.DIRS_FIRST, true));
		tabs.addTab("Dir Last", createJFileTreePanel(TreeType.DIRS_LAST, false));
		tabs.addTab("Dir Last Extensions", createJFileTreePanel(TreeType.DIRS_LAST, true));
		tabs.addTab("Dir Only", createJFileTreePanel(TreeType.DIRS_ONLY, false));
		tabs.addChangeListener(new ChangeListener() {
	        public void stateChanged(ChangeEvent e) {
	        	DataStore.clearStore();
	        }
	    });
		return tabs;
	}
	
	/**
	 * Creates a {@code JPanel} containing a {@code JFileTree} on a {@code JScrollPane}.
	 * 
	 * @param type The TreeType of the {@code JFileTree}
	 * @param showExtensions Whether the nodes in the {@code JFileTree} show extensions or not.
	 * @return A {@code JPanel} containing a {@code JFileTree} configuration based on parameters.
	 * @version 1.0
	 * @author Dan
	 */
	private JPanel createJFileTreePanel(TreeType type, boolean showExtensions) {
		JFileTree tree = new JFileTree(new ComputerTreeModel(type), type, showExtensions);
		JScrollPane scroll = new JScrollPane(tree);
		JPanel panel = new JPanel(new GridLayout(1, 1));
		panel.add(scroll);
		return panel;
	}
	
	/**
	 * Run the program
	 * 
	 * @param args Any program arguments
	 * @version 1.0
	 * @author Dan
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(() -> new JFileTreeExamples());
	}
}
