package filetree;

import javax.swing.JTree;
import javax.swing.tree.TreeModel;

import filetree.design.FileNameRenderer;
import filetree.design.TreeHandleUI;
import filetree.handlers.MouseHandler;
import filetree.model.ComputerTreeModel;
import filetree.model.JFileTreeDefaults;
import filetree.model.TreeType;

/**
 *
 * @version 1.0
 * @author Dan
 */
public class JFileTree extends JTree {
	/**
	 * 
	 * @version 1.0
	 * @author Dan
	 */
	private static final long serialVersionUID = -7292080979058170354L;
	
	private TreeType type;
	private boolean showingExtensions;
	
	/**
	 * 
	 * @version 1.0
	 * @author Dan
	 */
	public JFileTree() {
		this(new ComputerTreeModel(JFileTreeDefaults.DEFAULT_TYPE), JFileTreeDefaults.DEFAULT_TYPE, JFileTreeDefaults.SHOW_EXTENSIONS_DEFAULT);
	}
	
	/**
	 * 
	 * @param model
	 * @version 1.0
	 * @author Dan
	 */
	public JFileTree(TreeModel model) {
		this(model, JFileTreeDefaults.DEFAULT_TYPE, JFileTreeDefaults.SHOW_EXTENSIONS_DEFAULT);
	}
	
	/**
	 * 
	 * @param type
	 * @version 1.0
	 * @author Dan
	 */
	public JFileTree(TreeType type) {
		this(new ComputerTreeModel(type), type, JFileTreeDefaults.SHOW_EXTENSIONS_DEFAULT);
	}
	
	/**
	 * 
	 * @param model
	 * @param type
	 * @version 1.0
	 * @author Dan
	 */
	public JFileTree(TreeModel model, TreeType type) {
		this(model, type, JFileTreeDefaults.SHOW_EXTENSIONS_DEFAULT);
	}
	
	/**
	 * 
	 * @param model
	 * @param type
	 * @param showExtensions
	 * @version 1.0
	 * @author Dan
	 */
	public JFileTree(TreeModel model, TreeType type, boolean showExtensions) {
		super(model);
		
		this.type = type;
		this.showingExtensions = showExtensions;
		
		setUI(new TreeHandleUI(this));
		setCellRenderer(new FileNameRenderer(type, showingExtensions));
		
		setRootVisible(false); //Allows for each drive to act as a root
		setShowsRootHandles(true); //Shows handles for each individual drive
		
		addMouseListener(new MouseHandler(this));
	}
	
	/**
	 * 
	 * @return
	 * @version 1.0
	 * @author Dan
	 */
	public TreeType getType() {
		return type;
	}
	
	/**
	 * 
	 * @return
	 * @version 1.0
	 * @author Dan
	 */
	public boolean getShowingExtensions() {
		return showingExtensions;
	}
}
