package filetree.model;

public class JFileTreeDefaults {
	/**
	 * 
	 */
	public final static TreeType DEFAULT_TYPE = TreeType.DIRS_FIRST;
	
	/**
	 * 
	 */
	public final static boolean SHOW_EXTENSIONS_DEFAULT = false;
	
	/**
	 * 
	 * @version 1.0
	 * @author Dan
	 */
	public static int DEFAULT_HANDLE_FADE_TIME = 300;
	
	/**
	 * 
	 * @version 1.0
	 * @author Dan
	 */
	public static boolean DEFAULT_SHOW_ABSOLUTE_PATH = false;
	
	/**
	 * 
	 * @version 1.0
	 * @author Dan
	 */
	public static boolean DEFAULT_SHOW_ABSOLUTE_PATH_ROOT = false;
}
