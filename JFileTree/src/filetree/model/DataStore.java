package filetree.model;

import java.io.File;
import java.util.TreeMap;

import filetree.utility.JFileComparator;

/**
 *
 * @version 1.0
 * @author Dan
 */
public class DataStore {
	/**
	 * 
	 * @version 1.0
	 * @author Dan
	 */
	public static TreeMap<File, File[]> FILE_LIST_STORE = new TreeMap<>(new JFileComparator());

	/**
	 * 
	 * @version 1.0
	 * @author Dan
	 */
	public static TreeMap<File, Boolean> HAS_CHILD_STORE = new TreeMap<>(new JFileComparator());
	
	/**
	 * 
	 * @version 1.0
	 * @author Dan
	 */
	public static TreeMap<File, Boolean> IS_ROOT_STORE = new TreeMap<>(new JFileComparator());
	
	/**
	 * 
	 * @version 1.0
	 * @author Dan
	 */
	public static void clearStore() {
		FILE_LIST_STORE.clear();
		HAS_CHILD_STORE.clear();
		IS_ROOT_STORE.clear();
	}
}
