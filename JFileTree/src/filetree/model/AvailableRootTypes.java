package filetree.model;

import java.util.ArrayList;
import java.util.List;

public class AvailableRootTypes {
	/**
	 * 
	 * @version 1.0
	 * @author Dan
	 */
	public static List<String> AVAILABLE_ROOT_TYPES = new ArrayList<>();
	
	/**
	 * 
	 * @version 1.0
	 * @author Dan
	 */
	public static void setUpForWindows() {
		//There are more but only using these two for now
		AVAILABLE_ROOT_TYPES.add("Local Disk");
		AVAILABLE_ROOT_TYPES.add("USB Drive");
	}
	
	/**
	 * Not Used
	 * 
	 * @version 1.0
	 * @author Dan
	 */
	public static void setUpForMac() {
		//Don't own one, can't test
	}
	
	/**
	 * Not Used
	 * 
	 * @version 1.0
	 * @author Dan
	 */
	public static void setUpForLinux() {
		//Does Linux have these?
	}
}
