package filetree.model;

/**
 * 
 *
 * @version 1.0
 * @author Dan
 */
public enum TreeType {
	/**
	 * 
	 * @version 1.0
	 * @author Dan
	 */
	ALPHABETICAL,
	
	/**
	 * 
	 * @version 1.0
	 * @author Dan
	 */
	DIRS_FIRST,
	
	/**
	 * 
	 * @version 1.0
	 * @author Dan
	 */
	DIRS_LAST,
	
	/**
	 * 
	 * @version 1.0
	 * @author Dan
	 */
	DIRS_ONLY
}
