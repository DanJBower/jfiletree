package filetree.model;

import java.io.File;
import java.util.Arrays;
import java.util.List;

import javax.swing.filechooser.FileSystemView;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.TreeModel;
import javax.swing.tree.TreeNode;

import filetree.utility.CheckerMethods;

/**
 *
 * @version 1.0
 * @author Dan
 */
public class ComputerTreeModel implements TreeModel {
	private DefaultMutableTreeNode root = new DefaultMutableTreeNode();
	private TreeType type;
	
	/**
	 * 
	 * @version 1.0
	 * @author Dan
	 */
	public ComputerTreeModel() {
		this(JFileTreeDefaults.DEFAULT_TYPE, null);
	}
	
	/**
	 * 
	 * @param type
	 * @version 1.0
	 * @author Dan
	 */
	public ComputerTreeModel(TreeType type) {
		this(type, null);
	}
	
	/**
	 * 
	 * @param roots
	 * @version 1.0
	 * @author Dan
	 */
	public ComputerTreeModel(List<File> roots) {
		this(JFileTreeDefaults.DEFAULT_TYPE, roots);
	}
	
	/**
	 * 
	 * @param type
	 * @param roots
	 * @version 1.0
	 * @author Dan
	 */
	public ComputerTreeModel(TreeType type, List<File> roots) {
		this.type = type;
		createRootLayout(roots);
	}
	
	/**
	 * 
	 * @param roots
	 * @version 1.0
	 * @author Dan
	 */
	private void createRootLayout(List<File> roots) {
		if(roots == null || roots.isEmpty()) {
			for(File path : File.listRoots()) { //Loops through the "root" directories on a computer - Determined by system
				if(path.exists()) {
					if(addable(path)) {
						root.add(new DefaultMutableTreeNode(path));
					}
				}
			}
		} else {
			for(File path : roots) { //Loops through the "root" directories on a computer - Determined by system
				if(path.exists() && (CheckerMethods.isRoot(path) || path.isDirectory())) {
					root.add(new DefaultMutableTreeNode(path));
				}
			}
		}
	}
	
	/**
	 * 
	 * @param path
	 * @return
	 * @version 1.0
	 * @author Dan
	 */
	private boolean addable(File path) {
		if(AvailableRootTypes.AVAILABLE_ROOT_TYPES.size() > 0) {
			FileSystemView fsv = FileSystemView.getFileSystemView();
			String type = fsv.getSystemTypeDescription(path);
			if(type != null) {
				for(String s : AvailableRootTypes.AVAILABLE_ROOT_TYPES) {
					if(type.trim().equalsIgnoreCase(s.trim())) {
						return true;
					}
				}
			}
			return false;
		}
		return true;
	}

	/**
	 * Not Used
	 * @version 1.0
	 * @author Dan
	 */
	@Override
    public void addTreeModelListener(javax.swing.event.TreeModelListener l) {}
	
	/**
	 * 
	 * @version 1.0
	 * @author Dan
	 */
    @Override
    public Object getChild(Object parent, int index) {
    	if(type == TreeType.DIRS_ONLY) {
    		if(parent != root && parent instanceof DefaultMutableTreeNode) {
    			File f = (File) (((DefaultMutableTreeNode) parent).getUserObject());
    			return CheckerMethods.listDirectories(f)[index];
    		} else if(parent != root) {
    			File f = (File) parent;
    			return CheckerMethods.listDirectories(f)[index];
    		}
    		return root.getChildAt(index);
    	} else if(type == TreeType.ALPHABETICAL || type == TreeType.DIRS_FIRST || type == TreeType.DIRS_LAST) {
    		if(parent != root && parent instanceof DefaultMutableTreeNode) {
    			File f = (File) (((DefaultMutableTreeNode) parent).getUserObject());
    			return CheckerMethods.listFiles(f, type)[index];
    		} else if(parent != root) {
    			File f = (File) parent;
    			return CheckerMethods.listFiles(f, type)[index];
    		}
    		return root.getChildAt(index);
    	}
		return 0;
    }
    
    /**
     * 
	 * @version 1.0
	 * @author Dan
     */
    @Override
    public int getChildCount(Object parent) {
    	if(type == TreeType.DIRS_ONLY) {
			if(parent != root && parent instanceof DefaultMutableTreeNode) {
				File f = (File) (((DefaultMutableTreeNode) parent).getUserObject());
				if (!f.isDirectory()) {
					return 0;
				} else {
					return CheckerMethods.listDirectories(f).length;
				}
			} else if(parent != root) {
				File f = (File) parent;
				if (!f.isDirectory()) {
					return 0;
				} else {
					return CheckerMethods.listDirectories(f).length;
				}
			}
			return root.getChildCount();
    	} else if(type == TreeType.ALPHABETICAL || type == TreeType.DIRS_FIRST || type == TreeType.DIRS_LAST) {
    		if(parent != root && parent instanceof DefaultMutableTreeNode) {
				File f = (File) (((DefaultMutableTreeNode) parent).getUserObject());
				if (!f.isDirectory()) {
					return 0;
				} else {
					return CheckerMethods.listFiles(f, type).length;
				}
			} else if(parent != root) {
				File f = (File) parent;
				if (!f.isDirectory()) {
					return 0;
				} else {
					return CheckerMethods.listFiles(f, type).length;
				}
			}
			return root.getChildCount();
    	}
    	return 0;
    }
    
    /**
     * 
     * @param parent
     * @return
	 * @version 1.0
	 * @author Dan
     */
    public boolean hasChildren(Object parent) {
    	if(type == TreeType.DIRS_ONLY) {
	    	if(parent != root && parent instanceof DefaultMutableTreeNode) {
				return CheckerMethods.hasDirectory((File) (((DefaultMutableTreeNode) parent).getUserObject()));
			} else if(parent != root) {
				return CheckerMethods.hasDirectory((File) parent);
			}
			return root.getChildCount() != 0 ? true : false;
    	} else if(type == TreeType.ALPHABETICAL || type == TreeType.DIRS_FIRST || type == TreeType.DIRS_LAST) {
    		if(parent != root && parent instanceof DefaultMutableTreeNode) {
				return CheckerMethods.hasFile((File) (((DefaultMutableTreeNode) parent).getUserObject()));
			} else if(parent != root) {
				return CheckerMethods.hasFile((File) parent);
			}
			return root.getChildCount() != 0 ? true : false;
    	}
    	return false;
    }
    
    /**
     * 
	 * @version 1.0
	 * @author Dan
     */
    @Override
    public int getIndexOfChild(Object parent, Object child) {
    	if(type == TreeType.DIRS_ONLY) {
    		if(parent != root && parent instanceof DefaultMutableTreeNode) {
    			File par = (File) (((DefaultMutableTreeNode) parent).getUserObject());
    			File ch = (File) child;
    			return Arrays.asList(CheckerMethods.listDirectories(par)).indexOf(ch);
    		} else if(parent != root) {
    			File par = (File) parent;
    			File ch = (File) child;
    			return Arrays.asList(CheckerMethods.listDirectories(par)).indexOf(ch);
    		}
    		
    		return root.getIndex((TreeNode) child);
    	} else if(type == TreeType.ALPHABETICAL || type == TreeType.DIRS_FIRST || type == TreeType.DIRS_LAST) {
    		if(parent != root && parent instanceof DefaultMutableTreeNode) {
    			File par = (File) (((DefaultMutableTreeNode) parent).getUserObject());
    			File ch = (File) child;
    			return Arrays.asList(CheckerMethods.listFiles(par, type)).indexOf(ch);
    		} else if(parent != root) {
    			File par = (File) parent;
    			File ch = (File) child;
    			return Arrays.asList(CheckerMethods.listFiles(par, type)).indexOf(ch);
    		}
    		
    		return root.getIndex((TreeNode) child);
    	}
		return 0;
    }

    /**
     * 
	 * @version 1.0
	 * @author Dan
     */
    @Override
    public Object getRoot() {
        return root;
    }
    
    /**
     * 
	 * @version 1.0
	 * @author Dan
     */
    @Override
    public boolean isLeaf(Object node) {
    	if(type == TreeType.DIRS_ONLY) {
    		return false;
    	} else if(type == TreeType.ALPHABETICAL || type == TreeType.DIRS_FIRST || type == TreeType.DIRS_LAST) {
    		if(node instanceof DefaultMutableTreeNode) {
    			DefaultMutableTreeNode treeNode = (DefaultMutableTreeNode) node;
    			File file = (File)(treeNode.getUserObject());
    			if(file != null) {
    				if(file.isDirectory()) {
    					return false;
    				}
    				if(file.isFile()) {
    					return true;
    				}
    			}
    		} else if(node instanceof File) {
    			File file = (File) node;
    			if(file != null) {
    				if(file.isDirectory()) {
    					return false;
    				}
    				if(file.isFile()) {
    					return true;
    				}
    			}
    		}
    	}
		return false;
    }

    /**
     * Not Used
     * 
	 * @version 1.0
	 * @author Dan
     */
    @Override
    public void removeTreeModelListener(javax.swing.event.TreeModelListener l) {}
    
    /**
     * Not Used
     * 
	 * @version 1.0
	 * @author Dan
     */
    @Override
    public void valueForPathChanged(javax.swing.tree.TreePath path, Object newValue) {}
}
