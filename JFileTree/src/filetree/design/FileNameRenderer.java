package filetree.design;

import java.awt.Color;
import java.awt.Component;
import java.awt.Graphics;
import java.awt.Image;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.Arrays;

import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JTree;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeCellRenderer;

import filetree.model.JFileTreeDefaults;
import filetree.model.TreeType;
import filetree.utility.CheckerMethods;
import filetree.utility.FilenameUtils;
import sun.awt.shell.ShellFolder;

/**
 *
 * @version 1.0
 * @author Dan
 */
public class FileNameRenderer extends DefaultTreeCellRenderer {
	/**
	 * 
	 * @version 1.0
	 * @author Dan
	 */
	private static final long serialVersionUID = 9140320875927061882L;
	
	/**
	 * 
	 * @version 1.0
	 * @author Dan
	 */
	public static int DEFAULT_ICON_WIDTH = 16;
	
	/**
	 * 
	 * @version 1.0
	 * @author Dan
	 */
	public static int DEFAULT_ICON_HEIGHT = 16;
	
	private boolean mOver = false;
	private boolean selected = false;
	private Color rolloverColor = null;
	private TreeType type;
	private boolean showingExtensions;
	
	private ImageIcon rootIcon = new ImageIcon("resources/root.png");
	private ImageIcon openFolderIcon = new ImageIcon("resources/open-folder.png");
	private ImageIcon folderIcon = new ImageIcon("resources/folder.png");
	
	/**
	 * 
	 * @param type
	 * @param showExtensions
	 * @version 1.0
	 * @author Dan
	 */
	public FileNameRenderer(TreeType type, boolean showExtensions) {
		this.type = type;
		this.showingExtensions = showExtensions;
		setBackgroundSelectionColor(new Color(205, 232, 255));
		setHoverSelectionColor(new Color(229, 243, 255));
	}
	
	/**
	 * 
	 * @param rollover
	 * @version 1.0
	 * @author Dan
	 */
	public void setHoverSelectionColor(Color rollover) {
		rolloverColor = rollover;
	}
	
	/**
	 * 
	 * @return
	 * @version 1.0
	 * @author Dan
	 */
	public Color getHoverSelectionColor() {
		return rolloverColor;
	}
	
	/**
	 * 
	 * @version 1.0
	 * @author Dan
	 */
	@Override
	public Component getTreeCellRendererComponent(JTree tree, Object value, boolean sel, boolean expanded, boolean leaf, int row, boolean hasFocus) {
		return getTreeCellRendererComponent(tree, value, sel, expanded, leaf, row, hasFocus, false);
	}
	
	/**
	 * Replaces the node text from the path of the directory to just the directory name
	 * Also update the what node is selected and whether the mouse is over it for other
	 * functions in this class
	 * Sets some of the icons of the nodes on the tree. Ie if the directory is expanded
	 * the icon will change
	 * 
	 * @param tree
	 * @param value
	 * @param sel
	 * @param expanded
	 * @param leaf
	 * @param row
	 * @param hasFocus
	 * @param mouseOver
	 * @return
	 * @version 1.0
	 * @author Dan
	 */
	public Component getTreeCellRendererComponent(JTree tree, Object value, boolean sel, boolean expanded, boolean leaf, int row, boolean hasFocus, boolean mouseOver) {
		File holder = null;
		if(value instanceof File) {
			holder = (File) value;
			if(!Arrays.asList(File.listRoots()).contains(holder)) {
				String baseName = FilenameUtils.getBaseName(holder.getAbsolutePath()).trim();
				if(showingExtensions || baseName.equals("")) {
					if(!JFileTreeDefaults.DEFAULT_SHOW_ABSOLUTE_PATH) {
						value = holder.getName();
					}
				} else {
					if(!JFileTreeDefaults.DEFAULT_SHOW_ABSOLUTE_PATH) {
						value = FilenameUtils.getBaseName(holder.getAbsolutePath());
					} else {
						value = FilenameUtils.getFullPath(holder.getAbsolutePath()) + FilenameUtils.getBaseName(holder.getAbsolutePath());
					}
				}
			}
		} else if(value instanceof DefaultMutableTreeNode) {
			if(((DefaultMutableTreeNode) value).getUserObject() != null) {
				holder = (File) ((DefaultMutableTreeNode) value).getUserObject();
				if(!Arrays.asList(File.listRoots()).contains(holder)) {
					String baseName = FilenameUtils.getBaseName(holder.getAbsolutePath()).trim();
					if(showingExtensions || baseName.equals("")) {
						if(!JFileTreeDefaults.DEFAULT_SHOW_ABSOLUTE_PATH_ROOT) {
							value = holder.getName();
						}
					} else {
						if(!JFileTreeDefaults.DEFAULT_SHOW_ABSOLUTE_PATH_ROOT) {
							value = FilenameUtils.getBaseName(holder.getAbsolutePath());
						} else {
							value = FilenameUtils.getFullPath(holder.getAbsolutePath()) + FilenameUtils.getBaseName(holder.getAbsolutePath());
						}
					}
				}
			}
		}
		
		JComponent c = (JComponent) super.getTreeCellRendererComponent(tree, value, sel, expanded, leaf, row, hasFocus);	
		selected = sel;
		mOver = mouseOver;
		
		if(c instanceof JLabel) {
			((JLabel) c).setText("<html><p style=\"padding: 3px 0;\">" + value + "<p/>");
		}
		
		if(type == TreeType.DIRS_ONLY) {
			if(value instanceof DefaultMutableTreeNode) {
				if(((DefaultMutableTreeNode) value).getUserObject() != null) {
					if(CheckerMethods.isRoot((File) ((DefaultMutableTreeNode) value).getUserObject())) {
						setIcon(rootIcon);
					} else if(expanded && CheckerMethods.hasDirectory((File) ((DefaultMutableTreeNode) value).getUserObject())) {
						setIcon(openFolderIcon);
					} else {
						setIcon(folderIcon);
					}
				}
			} else if(expanded && holder != null && CheckerMethods.hasDirectory(holder)) {
				setIcon(openFolderIcon);
			} else {
				setIcon(folderIcon);
			}
		} else if(type == TreeType.ALPHABETICAL || type == TreeType.DIRS_FIRST || type == TreeType.DIRS_LAST) {
			if(value instanceof DefaultMutableTreeNode) {
				if(((DefaultMutableTreeNode) value).getUserObject() != null) {
					if(CheckerMethods.isRoot((File) ((DefaultMutableTreeNode) value).getUserObject())) {
						setIcon(rootIcon);
					} else if(expanded && CheckerMethods.hasFile((File) ((DefaultMutableTreeNode) value).getUserObject())) {
						setIcon(openFolderIcon);
					} else {
						setIcon(folderIcon);
					}
				}
			} else if(expanded && holder != null && CheckerMethods.hasFile(holder)) {
				setIcon(openFolderIcon);
			} else if(holder.isDirectory()) {
				setIcon(folderIcon);
			} else if(holder.isFile()) {
				ShellFolder sf = null;
				try {
					sf = ShellFolder.getShellFolder(holder);
				} catch (FileNotFoundException e) {}
				
				if(sf != null) {
					if(sf.getIcon(true) != null) {
					    ImageIcon imageIcon = new ImageIcon(sf.getIcon(true)); // load the image to a imageIcon
					    Image image = imageIcon.getImage(); // transform it 
					    Image newimg = image.getScaledInstance(DEFAULT_ICON_WIDTH, DEFAULT_ICON_HEIGHT,  java.awt.Image.SCALE_SMOOTH); // scale it the smooth way  
					    imageIcon = new ImageIcon(newimg);  // transform it back
					    
					    setIcon(imageIcon);
					}
				}
			}
		}
		
		return c;
	}
	
	//Paints the node
	//Changes background colour of the node if the mouse is over it
	//or if it is selected
	//Offsets paint by so much so that the node works with the icons
	/**
	 * 
	 * @version 1.0
	 * @author Dan
	 */
	@Override
	public void paintComponent(Graphics g) {
		Color bColor;

		if (selected) {
			bColor = getBackgroundSelectionColor();
		} else if(mOver) {
			bColor = getHoverSelectionColor();
		} else {
			bColor = getBackgroundNonSelectionColor();
			if (bColor == null) {
				bColor = getBackground();
			}
		}
		
		int imageOffset = -1;
		if (bColor != null) {
			imageOffset = getLabelStart();
			g.setColor(bColor);
			if(getComponentOrientation().isLeftToRight()) {
				g.fillRect(imageOffset, 0, getWidth() - imageOffset, getHeight());
			} else {
				g.fillRect(0, 0, getWidth() - imageOffset, getHeight());
			}
		}
		
		super.paintComponent(g);
	}
	
	//Used for getting the image offset for the paintComponent function
	/**
	 * 
	 * @return
	 * @version 1.0
	 * @author Dan
	 */
	private int getLabelStart() {
		Icon currentI = getIcon();
		if(currentI != null && getText() != null) {
			return currentI.getIconWidth() + Math.max(0, getIconTextGap() - 1);
		}
		return 0;
	}
}
