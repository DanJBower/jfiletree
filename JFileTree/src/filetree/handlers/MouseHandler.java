package filetree.handlers;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.JTree;
import javax.swing.SwingUtilities;

import filetree.design.TreeHandleUI;
import filetree.model.JFileTreeDefaults;

/**
 *
 * @version 1.0
 * @author Dan
 */
public class MouseHandler extends MouseAdapter {
	private JTree tree = null;
	private TreeHandleUI tUI = null;
	
	/**
	 * 
	 * @param tree
	 * @version 1.0
	 * @author Dan
	 */
	public MouseHandler(JTree tree) {
		this.tree = tree;
		tUI = (TreeHandleUI) tree.getUI();
	}
	
	/**
	 * 
	 * @version 1.0
	 * @author Dan
	 */
	@Override
	public void mouseEntered(MouseEvent e) {
		tUI.fadeHandles(true, JFileTreeDefaults.DEFAULT_HANDLE_FADE_TIME); //Fades handles in
		tUI.setMouseIn(true);
	}
	
	/**
	 * 
	 * @version 1.0
	 * @author Dan
	 */
	@Override
	public void mouseExited(MouseEvent e) {
		tUI.fadeHandles(false, JFileTreeDefaults.DEFAULT_HANDLE_FADE_TIME); //Fades handles out
		tUI.setMouseIn(false);
	}
	
	/**
	 * 
	 * @version 1.0
	 * @author Dan
	 */
	@Override
	public void mousePressed(MouseEvent e) {
		//Clears selection when mouse is pressed outside of on of the nodes
		if((SwingUtilities.isLeftMouseButton(e) || SwingUtilities.isRightMouseButton(e))
				&& tree.getRowForLocation(e.getX(), e.getY()) == -1) {
			tree.clearSelection();
		}
	}
}
