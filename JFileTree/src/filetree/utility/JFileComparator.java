/*
 * The Alphanum Algorithm is an improved sorting algorithm for strings
 * containing numbers.  Instead of sorting numbers in ASCII order like
 * a standard sort, this algorithm sorts numbers in numeric order.
 *
 * The Alphanum Algorithm is discussed at http://www.DaveKoelle.com
 *
 * Released under the MIT License - https://opensource.org/licenses/MIT
 *
 * Copyright 2007-2017 David Koelle
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included
 * in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE
 * USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package filetree.utility;

import java.io.File;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

/**
 * Modified from the AlphanumComparator
 * 
 * @version 1.0
 * @author Dan
 * @see http://www.davekoelle.com/files/AlphanumComparator.java
 */
public class JFileComparator implements Comparator<File> {
	/**
	 * 
	 * @version 1.0
	 * @author Dan
	 */
	private static final SortOrder DEFAULT_SORT_ORDER = SortOrder.DIRECTORIES_FIRST;
	
	private SortOrder sortOrder;
	
	/**
	 * 
	 * @version 1.0
	 * @author Dan
	 */
	public enum SortOrder {
		/**
		 * 
		 * @version 1.0
		 * @author Dan
		 */
		ALPHABETICAL,
		
		/**
		 * 
		 * @version 1.0
		 * @author Dan
		 */
		DIRECTORIES_FIRST,
		
		/**
		 * 
		 * @version 1.0
		 * @author Dan
		 */
		DIRECTORIES_LAST
	}

	/**
	 * 
	 * @version 1.0
	 * @author Dan
	 */
	public JFileComparator() {
		this(DEFAULT_SORT_ORDER);
	}
	
	/**
	 * 
	 * @version 1.0
	 * @author Dan
	 */
	public JFileComparator(SortOrder sortOrder) {
		this.setSortOrder(sortOrder);
	}

	/**
	 * 
	 * @return
	 * @version 1.0
	 * @author Dan
	 */
	public SortOrder getSortOrder() {
		return sortOrder;
	}

	/**
	 * 
	 * @param sortOrder
	 * @version 1.0
	 * @author Dan
	 */
	public void setSortOrder(SortOrder sortOrder) {
		this.sortOrder = sortOrder;
	}

	/**
	 * 
	 * @param ch
	 * @return
	 * @author David Koelle
	 */
	private final boolean isDigit(char ch) {
        return ((ch >= 48) && (ch <= 57));
    }

	/**
	 * 
	 * @author Dan
	 * @author David Koelle
	 * @version 1.0
	 */
    public int compare(File f1, File f2) {
    	if (f1 == null || f2 == null) {
    		return 0;
    	}
    	
    	if(sortOrder != SortOrder.ALPHABETICAL) {
        	if(f1.isDirectory() && f2.isFile()) {
        		return sortOrder == SortOrder.DIRECTORIES_FIRST ? -1 : 1;
        	} else if(f1.isFile() && f2.isDirectory()) {
        		return sortOrder == SortOrder.DIRECTORIES_FIRST ? 1 : -1;
        	}
    	}

        List<String> filePathOneList = getFilePathParts(f1);
        List<String> filePathTwoList = getFilePathParts(f2);
    	
        if(filePathOneList.size() == 0) {
        	if(filePathTwoList.size() == 0) {
        		return 0;
        	}
        	
        	return -1;
        }
        
        if(filePathTwoList.size() == 0) {
        	if(filePathOneList.size() == 0) {
        		return 0;
        	}
        	
        	return 1;
        }
        
    	int length = filePathOneList.size() > filePathTwoList.size() ? filePathTwoList.size() : filePathOneList.size();
    	
    	for(int i = 0; i < length; i++) {
    		int compareValue = CompareString(filePathOneList.get(i), filePathTwoList.get(i));
    		if(compareValue != 0) {
    			return compareValue;
    		}
    	}
    	
    	return filePathOneList.size() - filePathTwoList.size();
    }
    
    /**
     * 
     * @param file
     * @return
	 * @version 1.0
	 * @author Dan
     */
    private List<String> getFilePathParts(File file) {
    	List<String> returnVal = new ArrayList<>();
    	String filePath = file.getAbsolutePath().toLowerCase();
    	
    	String[] seperateDrive = filePath.split(":\\\\");
    	returnVal.add(seperateDrive[0]);
    	
    	if(seperateDrive.length > 1) {
        	String[] seperatePath = seperateDrive[1].split("\\\\");
        	for(String s : seperatePath) {
        		returnVal.add(s);
        	}
    	}
    	
    	return returnVal;
    }
    
    /*
     * Length of string is passed in for improved efficiency (only need to calculate it once)
     */
    /**
     * 
     * @param s
     * @param slength
     * @param marker
     * @return
	 * @version 1.0
	 * @author David Koelle
     */
    private final String getChunk(String s, int slength, int marker) {
        StringBuilder chunk = new StringBuilder();
        char c = s.charAt(marker);
        chunk.append(c);
        marker++;
        if (isDigit(c)) {
            while (marker < slength) {
                c = s.charAt(marker);
                if (!isDigit(c)) {
                    break;
                }
                chunk.append(c);
                marker++;
            }
        } else {
            while (marker < slength) {
                c = s.charAt(marker);
                if (isDigit(c)) {
                    break;
                }
                chunk.append(c);
                marker++;
            }
        }
        return chunk.toString();
    }
    
    /**
     * 
     * @param s1
     * @param s2
     * @return
	 * @version 1.0
	 * @author David Koelle
     */
    private int CompareString(String s1, String s2) {
    	if (s1 == null || s2 == null) {
    		return 0;
    	}
    	
        int thisMarker = 0;
        int thatMarker = 0;
        int s1Length = s1.length();
        int s2Length = s2.length();

        while (thisMarker < s1Length && thatMarker < s2Length) {
            String thisChunk = getChunk(s1, s1Length, thisMarker);
            thisMarker += thisChunk.length();

            String thatChunk = getChunk(s2, s2Length, thatMarker);
            thatMarker += thatChunk.length();

            // If both chunks contain numeric characters, sort them numerically
            int result = 0;
            if (isDigit(thisChunk.charAt(0)) && isDigit(thatChunk.charAt(0))) {
                // Simple chunk comparison by length.
                int thisChunkLength = thisChunk.length();
                result = thisChunkLength - thatChunk.length();
                // If equal, the first different number counts
                if (result == 0) {
                    for (int i = 0; i < thisChunkLength; i++) {
                        result = thisChunk.charAt(i) - thatChunk.charAt(i);
                        if (result != 0) {
                            return result;
                        }
                    }
                }
            } else {
                result = thisChunk.compareTo(thatChunk);
            }

            if (result != 0) {
                return result;
            }
        }

        return s1Length - s2Length;
    }
}
