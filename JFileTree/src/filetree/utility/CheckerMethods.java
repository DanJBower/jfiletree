package filetree.utility;

import java.io.File;
import java.io.IOException;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

import filetree.model.DataStore;
import filetree.model.TreeType;

/**
 * 
 * @author Dan
 * @version 1.0
 */
public class CheckerMethods {
	/**
	 * 
	 * @param path
	 * @return
	 * @version 1.0
	 * @author Dan
	 */
	public static File[] listDirectories(File path) {
		File[] files = DataStore.FILE_LIST_STORE.get(path);
		
		if(files == null) {
			List<File> directories = new ArrayList<File>();
			for(File temp : path.listFiles()) {
				//Done like this instead of isDirectory as do not want to list dirs we can not access
				Path dir = Paths.get(temp.getAbsolutePath());
				DirectoryStream<Path> stream;
				try {
					stream = Files.newDirectoryStream(dir);
					stream.close();
					directories.add(temp);
				} catch (IOException e) {}
			}

			files = directories.toArray(new File[0]);
			DataStore.FILE_LIST_STORE.put(path, files);
			return files;
		}
		
		return files;
	}
	
	/**
	 * 
	 * @param path
	 * @param type
	 * @return
	 * @version 1.0
	 * @author Dan
	 */
	public static File[] listFiles(File path, TreeType type) {
		File[] files = DataStore.FILE_LIST_STORE.get(path);
		
		if(files == null) {
			List<File> allFiles = new ArrayList<File>();
			for(File temp : path.listFiles()) {
				if(temp.isDirectory()) {
					//Done like this instead of just isDirectory as do not want to list dirs we can not access
					Path dir = Paths.get(temp.getAbsolutePath());
					DirectoryStream<Path> stream;
					try {
						stream = Files.newDirectoryStream(dir);
						stream.close();
						allFiles.add(temp);
					} catch (IOException e) {}
				} else if(temp.isFile()) {
					allFiles.add(temp);
				}
			}
			
			if(type == TreeType.ALPHABETICAL || type == TreeType.DIRS_ONLY) {
				allFiles.sort(new JFileComparator(JFileComparator.SortOrder.ALPHABETICAL));
			} else if(type == TreeType.DIRS_FIRST) {
				allFiles.sort(new JFileComparator(JFileComparator.SortOrder.DIRECTORIES_FIRST));
			} else if(type == TreeType.DIRS_LAST) {
				allFiles.sort(new JFileComparator(JFileComparator.SortOrder.DIRECTORIES_LAST));
			}
			
			files = allFiles.toArray(new File[0]);
			DataStore.FILE_LIST_STORE.put(path, files);
			return files;
		}
		
		return files;
	}
	
	/**
	 * 
	 * @param path
	 * @return
	 * @version 1.0
	 * @author Dan
	 */
	public static boolean hasDirectory(File path) {
		System.out.println(path == null);
		Boolean hasChild = DataStore.HAS_CHILD_STORE.get(path);
		
		if(hasChild == null) {
			Path dir = Paths.get(path.getAbsolutePath());
			DirectoryStream<Path> stream;
			try {
				stream = Files.newDirectoryStream(dir);
				for(Path p : stream) {
					DirectoryStream<Path> tempStream;
					try {
						tempStream = Files.newDirectoryStream(p);
						tempStream.close();
						DataStore.HAS_CHILD_STORE.put(path, true);
						return true;
					} catch (IOException e) {}
				}
				stream.close();
				DataStore.HAS_CHILD_STORE.put(path, false);
				return false;
			} catch (IOException e) {
				DataStore.HAS_CHILD_STORE.put(path, false);
				return false;
			}
		}
		
		return hasChild;
	}
	
	/**
	 * 
	 * @param path
	 * @return
	 * @version 1.0
	 * @author Dan
	 */
	public static boolean hasFile(File path) {
		Boolean hasChild = DataStore.HAS_CHILD_STORE.get(path);
		
		if(hasChild == null) {
			if(path.isDirectory()) {
				Path dir = Paths.get(path.getAbsolutePath());
				DirectoryStream<Path> stream;
				try {
					stream = Files.newDirectoryStream(dir);
					boolean returnVal = stream.iterator().hasNext();
					stream.close();
					DataStore.HAS_CHILD_STORE.put(path, returnVal);
					return returnVal;
				} catch (IOException e) {
					DataStore.HAS_CHILD_STORE.put(path, false);
					return false;
				}
			}
			DataStore.HAS_CHILD_STORE.put(path, false);
			return false;
		}
		
		return hasChild;
	}
	
	public static boolean isRoot(File root) {
		Boolean hasChild = DataStore.IS_ROOT_STORE.get(root);
		
		if(hasChild == null) {
			for(File path : File.listRoots()) { //Loops through the "root" directories on a computer - Determined by system
				if(path != null && path.exists() && root != null && root.exists()) {
					if(path.getAbsolutePath().equals(root.getAbsolutePath())) {
						DataStore.IS_ROOT_STORE.put(root, true);
						return true;
					}
				}
			}
			DataStore.IS_ROOT_STORE.put(root, false);
			return false;
		}
		
		return hasChild;
	}
}
