# JFileTree #
### What is this repository for? ###
`JFileTree` is an extension of Java's `JTree`. It is designed to list a computers file structure from a defined root. It will not list directories or files that you need elevated permissions to see normally.

![enter image description here](https://i.imgur.com/468G4ik.png?1)

### Basic Usage ###
To use the default values, mostly set in `JFileTreeDefaults`, you can call `JFileTree` as shown below.

    JFileTree tree = new JFileTree();

For more complex configurations see below.

### Configurations ###
#### Roots ####
You can make no configurations for this. This will list all the system roots. For example, on Windows, it might list `C:\` and `D:\`.
___
You can define the types of system roots to specify what types of root file is listed. There are two ways to do this. You can use an inbuilt method on `AvailableRootTypes` such as `setUpForWindows()` or you can manually add the types you want to add. For example,

    AvailableRootTypes.AVAILABLE_ROOT_TYPES.add("Local Disk");
	AvailableRootTypes.AVAILABLE_ROOT_TYPES.add("USB Drive");

Or you could do both. **This has to be done before defining the `JFileTree`.**
___
Finally you can define a custom list of directories or drives to use as roots. For example,

    List<File> customRoots = new ArrayList<>();
	customRoots.add(new File("C:\\"));
	customRoots.add(new File("C:\\Users"));
		
	JFileTree tree = new JFileTree(new ComputerTreeModel(customRoots));

#### Type ####
There are four types of `JFileTree`. These are defined in `TreeType` as

 * `ALPHABETICAL`: List all the files and directories in alphabetical order.
 * `DIRS_FIRST`: List all the directories in alphabetical order, then list all the files in alphabetical order.
 * `DIRS_LAST`: List all the files in alphabetical order, then list all the directories in alphabetical order.
 * `DIRS_ONLY`: List only the directories.

The way to make this configuration can be seen below.

    JFileTree tree = new JFileTree(type);

For example,

	JFileTree tree = new JFileTree(TreeType.DIRS_FIRST);

#### Extensions ####
You can choose to show or not show file extensions. They are always shown if a file has no name.

The way to make this configuration can be seen below.

    JFileTree tree = new JFileTree(new ComputerTreeModel(type), type, showExtensions);

For example,

	JFileTree tree = new JFileTree(new ComputerTreeModel(type), type, true);
